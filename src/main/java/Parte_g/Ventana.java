/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Parte_g;

/**
 *
 * @author Mauro
 */
public class Ventana extends javax.swing.JFrame {

    /**
     * Creates new form Ventana
     */
    public Ventana() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        ButtonFibo = new javax.swing.JButton();
        ButtonPrimo = new javax.swing.JButton();
        ButtonFact = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Operaciones Matematicas");

        jPanel1.setBackground(new java.awt.Color(153, 255, 153));

        ButtonFibo.setFont(new java.awt.Font("Forte", 0, 14)); // NOI18N
        ButtonFibo.setText("Fibonacci");
        ButtonFibo.setToolTipText("");
        ButtonFibo.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                ButtonFiboMouseClicked(evt);
            }
        });
        ButtonFibo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonFiboActionPerformed(evt);
            }
        });

        ButtonPrimo.setFont(new java.awt.Font("Forte", 0, 14)); // NOI18N
        ButtonPrimo.setText("Determinar Primo");
        ButtonPrimo.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                ButtonPrimoMouseClicked(evt);
            }
        });
        ButtonPrimo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonPrimoActionPerformed(evt);
            }
        });

        ButtonFact.setFont(new java.awt.Font("Forte", 0, 14)); // NOI18N
        ButtonFact.setText("Calcular Factorial");
        ButtonFact.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                ButtonFactMouseClicked(evt);
            }
        });
        ButtonFact.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonFactActionPerformed(evt);
            }
        });

        jLabel1.setBackground(new java.awt.Color(255, 255, 255));
        jLabel1.setFont(new java.awt.Font("Forte", 0, 24)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(0, 0, 0));
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Calculo de Operaciones Matematicas");
        jLabel1.setToolTipText("");
        jLabel1.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(ButtonFibo, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(83, 83, 83)
                .addComponent(ButtonPrimo, javax.swing.GroupLayout.DEFAULT_SIZE, 130, Short.MAX_VALUE)
                .addGap(83, 83, 83)
                .addComponent(ButtonFact, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(49, Short.MAX_VALUE))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(128, 128, 128)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 439, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(40, Short.MAX_VALUE)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(31, 31, 31)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(ButtonFact, javax.swing.GroupLayout.DEFAULT_SIZE, 57, Short.MAX_VALUE)
                    .addComponent(ButtonFibo, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 57, Short.MAX_VALUE)
                    .addComponent(ButtonPrimo, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 57, Short.MAX_VALUE))
                .addGap(123, 123, 123))
        );

        ButtonPrimo.getAccessibleContext().setAccessibleDescription("");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void ButtonPrimoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonPrimoActionPerformed

    }//GEN-LAST:event_ButtonPrimoActionPerformed

    private void ButtonFactActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonFactActionPerformed

    }//GEN-LAST:event_ButtonFactActionPerformed

    private void ButtonFiboActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonFiboActionPerformed

    }//GEN-LAST:event_ButtonFiboActionPerformed

    private void ButtonFiboMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_ButtonFiboMouseClicked
        Fibo f = new Fibo();
        f.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_ButtonFiboMouseClicked

    private void ButtonPrimoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_ButtonPrimoMouseClicked
        Primo p = new Primo();
        p.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_ButtonPrimoMouseClicked

    private void ButtonFactMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_ButtonFactMouseClicked
        Facto a = new Facto();
        a.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_ButtonFactMouseClicked

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton ButtonFact;
    private javax.swing.JButton ButtonFibo;
    private javax.swing.JButton ButtonPrimo;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    // End of variables declaration//GEN-END:variables
}
